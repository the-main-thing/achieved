import express from 'express'

/**
 * @param {Object} object - Object to filter
 * @param {function} filterFn - Function to apply to each element
 * @param {(boolean|undefined)} recursive - Optional. Recursively
 * filter nested objects. If nested object is empty, it will be removed.
 * @returns {Object} Object - New, filtered object.
 */
export const filterObject = (
	obj: Record<string, any>,
	filterFn: (obj: Record<string, any>) => boolean,
	recursive?: boolean
) => {
	const filteredObject: Record<string, any> = {}
	// For all keys in object
	for (const key of Object.keys(obj)) {
		let value = obj[key]
		// Apply filter to value
		if (recursive) {
			if (typeof value === 'object') {
				value = filterObject(filterFn, value, recursive)
				// emty objects must be removed
				if (Object.keys(value).length === 0) {
					value = undefined
				}
			}
		}
		const keep = filterFn(value)
		if (keep) {
			filteredObject[key] = value
		}
	}
	return filteredObject
}

export const asyncMiddleware = (fn: (...args: any[]) => Promise<any>) => (
	req: express.Request,
	res: express.Response,
	next: express.NextFunction
) => {
	Promise.resolve(fn(req, res, next)).catch(next)
}

export const commonGraphqlResolver = (db, parameter, resolver) => {
	if (!db || !parameter) {
		return
	}
	return resolver(db, parameter)
}
