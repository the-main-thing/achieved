import path from 'path'
import express from 'express'

import dbConnect from './arangodb'
import auth from './auth'
import createGraphQlServer from './graphql'

const app = express()
const port = process.env.PORT || 4000

async function startServer() {
	const db = await dbConnect()
	if (!db) {
		console.error(`Can't connect to database.\nExit`)
		process.exit(1)
	}

	// Add authentication mechanism
	auth(app, db)
	// Add graphql endpoint
	createGraphQlServer(app, db)

	// TODO: change it to something more convenient
	app.use(express.static(path.join(__dirname, '../../../public')))

	// Start the server
	app.listen({ port }, () => console.log('Server started at port', port))
}

startServer()
