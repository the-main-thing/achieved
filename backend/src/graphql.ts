import {
	gql,
	AuthenticationError,
	UserInputError,
	ApolloServer,
} from 'apollo-server-express'
import { Database } from 'arangojs'
import express from 'express'

import { UNAUTHORIZED, AUTHORIZED, Auth, AuthStep } from './auth'
import { getAchievments, upsertAchievment, ArangoDocument } from './arangodb'

export interface User extends ArangoDocument {
	_key: string
	_id: string
	email: string
	date: number
}

export interface Achievement {
	_key: string
	_id: string
	title: string
	date: number
	description?: string
}

export interface GraphQLAuth {
	step: AuthStep
	emailRecieved: boolean
	passwordSent: boolean
}

interface Context extends Auth {
	db: Database
}

const typeDefs = gql`
	type Achievement {
		_key: String!
		title: String!
		date: Int!
		description: String
	}

	type Auth {
		step: String!
		emailRecieved: Boolean!
		passwordSent: Boolean!
	}

	type Query {
		achievements: [Achievement]
		"""
		Possible values for auth step is:
		- UNAUTHORIZED
		- AUTHORIZED
		- NO_EMAIL
		- NO_PASSWORD
		- INVALID_EMAIL
		- INVALID_PASSWORD

		Email and password must be sent within
		body inside a POST request to any endpoint
		on the server.
		"""
		auth: Auth!
	}

	type Mutation {
		addAchievement(title: String!, description: String, date: Int): Achievement!
		"""
		TODO: Implement this
		Set user's email. This is for logging in. Returns authStep value
		"""
		email(email: String!): Auth!
		"""
		TODO: Implement this
		Set temporary password to log in. Returns authStep value.
		"""
		password(password: String!): String!
	}
`

const resolvers = {
	Query: {
		achievements: async (parent, args, context) => {
			const { user, authStep, db } = context as Context
			if (!user || authStep !== AUTHORIZED) {
				throw new AuthenticationError('must authenticate')
			}
			// get achievements
			const achievments = await getAchievments(db, user)
			if (!achievments) {
				throw new Error('database error: cannot get achievments')
			}
			return achievments
		},

		auth: (parent, args, context): GraphQLAuth => ({
			step: context.authStep,
			emailRecieved: context.user && context.user.email,
			passwordSent: Boolean(context.password),
		}),
	},

	Mutation: {
		addAchievement: async (parent, args, context) => {
			const { user, authStep, db } = context as Context
			if (!user || authStep !== AUTHORIZED) {
				throw new AuthenticationError('must authenticate')
			}
			const { title, description, date } = args
			if (!title) {
				throw new UserInputError('must provide a title')
			}
			const achievment = {
				title,
				description,
				date: date || new Date().getTime(),
			}
			const result = await upsertAchievment(db, user, achievment)
			if (!result) {
				throw new Error('database error: cannot add achievement')
			}
			return result
		},

		// TODO: implement auth logic
		email: (parent, { email }, { token, db }): GraphQLAuth => {
			return {
				step: UNAUTHORIZED,
				emailRecieved: email === 'hello',
				passwordSent: true,
			}
		},

		// TODO: implement auth logic
		password: (parent, { password }, { token, db }) => password,
	},
}

const createGraphQlServer = (app: express.Application, db: Database) => {
	const apolloServer = new ApolloServer({
		typeDefs,
		resolvers,
		context: ({ req }) => {
			const { user, authStep } = (req as any).auth as Auth
			return { user, authStep, db }
		},
		formatError: (err: any) => {
			console.error(`APOLLO ERROR:\n`, err)
			if (err.message.startsWith('database error')) {
				return new Error('Internal server error')
			}
			return err
		},
	})
	apolloServer.applyMiddleware({ app })
}

export default createGraphQlServer
