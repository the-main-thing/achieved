// IMPORTANT TODOs:
// - Handle bruteforce

// TODO: Move logic to graphql or make it more abstract, so it can be
// independent from express
// TODO: Error handling. Also on client

import express from 'express'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import { validate } from 'email-validator'
import nanoid from 'nanoid'
import generate from 'nanoid/generate'
import jwt from 'jsonwebtoken'
// Just for types
import { Database } from 'arangojs'

import { asyncMiddleware } from './helpers'
import { getUserByEmail, createUser, ArangoDocument } from './arangodb'

interface User extends ArangoDocument {
	email?: string
}

/* General authentication statuses */
export const UNAUTHORIZED = 'UNAUTHORIZED'
export const AUTHORIZED = 'AUTHORIZED'

/* Detailed authentication statuses */
// means we are meet the client and waiting for email
export const NO_EMAIL = 'NO_EMAIL'
// means we got the email, send password, and waiting password from client
export const NO_PASSWORD = 'NO_PASSWORD'
export const INVALID_EMAIL = 'INVALID_EMAIL'
export const INVALID_PASSWORD = 'INVALID_PASSWORD'

export type AuthStep =
	| 'UNAUTHORIZED'
	| 'AUTHORIZED'
	| 'NO_EMAIL'
	| 'NO_PASSWORD'
	| 'INVALID_EMAIL'
	| 'INVALID_PASSWORD'

// Object that auth function adds to the request object
export interface Auth {
	user?: User
	authStep: AuthStep
	password?: string
}

/**
 * Add several middlewares to an express app.
 * Auth middleware creates auth object: { user, authStep }
 * and adds this object to the request object.
 */
export default function auth(app: express.Application, db: Database) {
	if (process.env.NODE_ENV !== 'production') {
		app.use(cors())
	}
	app.use(bodyParser.json())
	// app.use(bodyParser.urlencoded({ extended: false }))
	app.use(cookieParser())
	app.use(
		asyncMiddleware(async (req, res, next) => {
			const secret = process.env.JWT_SECRET_KEY || nanoid(256)

			// Authentication calculations result handler
			const handleResult = (result: Auth, expiresIn?: string | number) => {
				// eslint-disable-next-line @typescript-eslint/no-unused-vars
				const { password, ...rest } = result
				// We do not store passord anywhere except jwt
				req.auth = rest
				const token = encodeToken(result, secret, expiresIn)
				const today = new Date()
				const threeDaysAfter = new Date(
					today.getTime() + 1000 * 60 * 60 * 24 * 3
				)
				// We store jwt inside http only cookie
				res.cookie('token', token, {
					exires: threeDaysAfter,
					httpOnly: true,
				})
				next()
			}

			const { email, password } = req.body
			const tokenCookie = req.cookies['token']
			const decodedToken = decodeToken(tokenCookie, secret)

			if (!decodedToken) {
				return handleResult({
					authStep: UNAUTHORIZED,
				})
			}

			let { user } = decodedToken
			const { authStep } = decodedToken
			try {
				switch (authStep) {
					case UNAUTHORIZED:
					case NO_EMAIL:
					case INVALID_EMAIL:
						if (!email) {
							return handleResult({
								authStep: NO_EMAIL,
							})
						}
						user = await getOrCreateUser(db, email)
						if (!user) {
							return handleResult({
								authStep: INVALID_EMAIL,
							})
						}
						// Got the user, go to next step and ask for password
						return handleResult(
							{
								user,
								authStep: NO_PASSWORD,
								password: createPassword(),
							},
							'5m'
						)

					case NO_PASSWORD:
					case INVALID_PASSWORD:
						if (!password) {
							return handleResult(
								{
									user,
									authStep: NO_PASSWORD,
									password: decodedToken.password,
								},
								decodedToken.exp || 0
							)
						}

						if (!decodedToken.password) {
							throw new Error('JWT does not contain password')
						}

						// Incorrect password
						if (decodedToken.password !== password) {
							return handleResult(
								{
									user,
									authStep: INVALID_PASSWORD,
									password: decodedToken.password,
								},
								decodedToken.exp || 0
							)
						}
					// Password is correct, fall throwgh to the next step
					// eslint-disable-next-line no-fallthrough
					case AUTHORIZED:
						return handleResult({
							user,
							authStep: AUTHORIZED,
						})

					default:
						throw new Error('Invalid authStep value')
				}
			} catch (err) {
				console.error(`Auth error`, err)
				return res
					.status(500)
					.json({ message: 'Server error occured during authentication' })
			}
		})
	)
}

function encodeToken(
	payload: any,
	secret: string,
	expiresIn?: string | number
) {
	// Set expiration if not given
	expiresIn = expiresIn ? expiresIn : '5d'
	return jwt.sign(payload, secret, { expiresIn })
}

function decodeToken(token: string | undefined, secret: string | undefined) {
	if (typeof token !== 'string' || typeof secret !== 'string') return
	try {
		const decoded = jwt.verify(token, secret) as unknown
		if (typeof decoded !== 'object') {
			throw new Error('Unexpected token value')
		}
		return decoded as {
			user: User
			authStep: string
			password?: string
			exp: number
		}
	} catch (e) {
		return undefined
	}
}

async function getOrCreateUser(db, email) {
	if (!email || !validate(email)) {
		return undefined
	}
	let user = await getUserByEmail(db, email)
	if (!user) {
		user = await createUser(db, email)
		if (!user) {
			throw new Error('Could not create user')
		}
	}
	return user
}

function createPassword(size = 6) {
	const password = generate('0123456789', size)
	// TODO: send email with password
	console.log('Sending email:', password)
	return password
}
