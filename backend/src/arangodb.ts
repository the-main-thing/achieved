import { Database, aql } from 'arangojs'

import { filterObject } from './helpers'

// Collections names
const USERS = 'users'
const ACHIEVMENTS = 'achievements'
const USERS_TO_ACHIEVEMENTS = 'users_to_achievments'

const DB_NAME = process.env.DB_NAME || 'achieved'

export interface ArangoDocument extends Record<string, unknown> {
	_id: string
	_key: string
}

/**
 * @typedef {Object} Arango - ArangoDB database connection object.
 * Connect to database.
 * @returns {(Arango|undefined)} - Returns database object or undefined
 * if there is any error.
 */
const dbConnect = async (
	auth?: { user: string; password?: string },
	config?: Record<string, unknown>
) => {
	const db = new Database(config)
	const { user, password } = auth || { user: undefined, password: undefined }
	db.useBasicAuth(user, password)
	try {
		// Try to create the database
		const info = await db.createDatabase(DB_NAME)
		if (process.env.NODE_ENV !== 'production') {
			console.log('Creating database')
			console.log({ info })
		}
		// Double check
		if (info.error || !info.result) {
			throw new Error('Cannot create database (step 0)')
		}
		// Triple check
		const exists = await db.exists()
		if (!exists) {
			throw new Error('Cannot create database (step 1)')
		}
		// Everything is ok, db created.
		console.log('DB was not exists, created new one.')
		db.useDatabase(DB_NAME)
		return db
	} catch (error) {
		// If duplicate name there is already such database
		if (error.isArangoError && error.response.body.errorNum === 1207) {
			console.log('Database exists, connecting.')
			db.useDatabase(DB_NAME)
			return db

			// If some other ArangoDB error
		} else if (error.isArangoError) {
			console.error('ArangoDB error:')
			console.error('\n', 'error code:', error.response.body.errorNum, '\n')
			console.error(error.response.body)
			return undefined
		} else {
			console.error('Error during database connection')
			return undefined
		}
	}
}
export default dbConnect

const logError = error => {
	if (error.isArangoError) {
		console.error('ArangoDB error:')
		console.error('\n', 'error code:', error.response.body.errorNum, '\n')
		console.error(error.response.body)
	} else {
		console.error('Error during database operation:')
		console.error(error)
	}
}

const getCollection = async (
	db: Database,
	collectionName: string,
	edge = false
) => {
	const collection = edge
		? db.edgeCollection(collectionName)
		: db.collection(collectionName)
	const exists = await collection.exists()
	if (!exists) {
		try {
			const response = await collection.create()
			if (response.code !== 200) {
				console.error(response)
				throw new Error(`Cant create collection ${collectionName}`)
			}
			return edge
				? db.edgeCollection(collectionName)
				: db.collection(collectionName)
		} catch (error) {
			return logError(error)
		}
	}
	return collection
}

const getDocument = async (
	db: Database,
	collectionName: string,
	key: string,
	edge = false
) => {
	const collection = await getCollection(db, collectionName, edge)
	if (!collection) return
	try {
		return collection.document(key)
	} catch (error) {
		logError(error)
	}
}

const getDocumentBy = async (
	db: Database,
	collectionName: string,
	key: string,
	value: any,
	edge = false
): Promise<ArangoDocument | undefined> => {
	const collection = await getCollection(db, collectionName, edge)
	if (!collection) return
	try {
		const cursor = await db.query(aql`
			LET result = (
				FOR document IN ${collection}
					FILTER document.${key} == ${value}
					LIMIT 1
			)
      RETURN document
    `)
		const result = await cursor.next()
		return result
	} catch (error) {
		logError(error)
	}
}

const getVertices = async (
	db: Database,
	edgeCollectionName: string,
	startVertex: ArangoDocument
): Promise<ArangoDocument[] | undefined> => {
	const collection = await getCollection(db, edgeCollectionName, true)
	if (!collection) return
	try {
		const cursor = await db.query(aql`
      FOR v IN 1..1 OUTBOUND ${startVertex} ${collection} 
        RETURN v
    `)
		const result = await cursor.all()
		return result
	} catch (error) {
		logError(error)
	}
}

const createEdge = async (
	db: Database,
	collectionName: string,
	from: ArangoDocument,
	to: ArangoDocument
): Promise<ArangoDocument | undefined> => {
	const collection = await getCollection(db, collectionName, true)
	if (!collection) return
	try {
		const cursor = await db.query(aql`
      UPSERT { _from: ${from._id || from}, _to: ${to._id || to} } 
      INSERT { _from: ${from._id || from}, _to: ${to._id || to} } 
      UPDATE
      IN ${collection}
      RETURN NEW
    `)
		const result = await cursor.next()
		return result
	} catch (error) {
		logError(error)
	}
}

const upsertDocument = async (
	db: Database,
	collectionName: string,
	document: Record<string, any>
): Promise<ArangoDocument | undefined> => {
	// Remove empty props. Especially _key because we want db to create one.
	const { _key = null, ...docToUpdate } = filterObject(document, Boolean, true)

	// If !_key then do not add it
	const docToInsert = (() => {
		const date = new Date().getTime()
		if (_key) {
			return { _key, date, ...docToUpdate }
		} else {
			return { date, ...docToUpdate }
		}
	})()

	const collection = await getCollection(db, collectionName)
	if (!collection) return
	try {
		const cursor = await db.query(aql`
        UPSERT { _key: ${_key} }
        INSERT ${docToInsert}
        UPDATE ${docToUpdate} IN ${collection}
            RETURN NEW
      `)
		const doc = await cursor.next()
		if (!doc) throw new Error(`Cannot UPSERT document: ${document}`)
		return doc
	} catch (error) {
		logError(error)
	}
}

/**
 * Create function that will return a document from specified collection
 */
export const documentGetter = (collectionName: string, edge = false) => {
	return (db: Database, key: string) =>
		getDocument(db, collectionName, key, edge)
}

export const documentGetterBy = (collectionName: string, edge = false) => {
	return (db: Database, key: string, value: unknown) =>
		getDocumentBy(db, collectionName, key, value, edge)
}

export const verticesGetter = (edgeCollectionName: string) => {
	return (db: Database, document: ArangoDocument) => {
		return getVertices(db, edgeCollectionName, document)
	}
}

export const edgeCreator = (collectionName: string) => {
	return (db: Database, from: ArangoDocument, to: ArangoDocument) =>
		createEdge(db, collectionName, from, to)
}

/**
 * Create function that will create or update document in specified collection
 */
export const documentUpserter = (collectionName: string) => {
	return (db: Database, document: Record<string, any>) =>
		upsertDocument(db, collectionName, document)
}

/* Helper functions for specific needs */
export const createUser = (db: Database, email: string) => {
	return upsertDocument(db, USERS, { email })
}

export const getUserByEmail = (db: Database, email: string) => {
	return getDocumentBy(db, USERS, 'email', email)
}

export const upsertAchievment = async (
	db: Database,
	user: ArangoDocument,
	achievement: {
		title: string
		description?: string
		date: number
	}
) => {
	const dbAchievment = await upsertDocument(db, ACHIEVMENTS, achievement)
	if (!dbAchievment) return console.error(`Cant create achievement`)
	const usersToAchievments = await createEdge(
		db,
		USERS_TO_ACHIEVEMENTS,
		user,
		dbAchievment
	)
	if (!usersToAchievments) return console.error(`Cant link user to achievment`)
	return dbAchievment
}

export const getAchievments = (db: Database, user: ArangoDocument) => {
	return getVertices(db, USERS_TO_ACHIEVEMENTS, user)
}
