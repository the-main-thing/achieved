// If this test is passed, then the app is probably ok
// if this test is failed, then the app is definetly broken.
describe('app', () => {
	it('works', () => {
		cy.visit('/')
			.findByText(/войти/i)
			.click()
			.findByText(/закрыть/i)
			.click()
			.findByText(/войти/i)
	})
})
