import React, { useState, useEffect } from 'react'
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'

// TODO: Add error handling. Also on server
// TODO: rethink file structure
// create more visible space for shared code
import { GraphQLAuth } from '../../backend/src/graphql'

// Auth is going like so:
// 1) Get an error from server with UNAUTHORIZED code
// 2) Navigate to the index page
// 3) Open "login/signup" dialog on click
// 4) Show "enter email" input
// 5) After sumitting email, show "password" input
// 6) Tell user to check email for temporary password
// 7) Navigate to the app on success

interface Auth {
	auth: GraphQLAuth
}

const SEND_EMAIL = gql`
	mutation Email($email: String!) {
		auth: email(email: $email) {
			step
		}
	}
`

const SEND_PASSWORD = gql`
	mutation Password($password: String!) {
		auth: password(password: $password) {
			step
			passwordSent
		}
	}
`

interface SendEmail {
	onResponse: (step: string) => void
}

const SendEmail: React.FC<SendEmail> = ({ onResponse }) => {
	const [email, setEmail] = useState('')
	const [state, setState] = useState('IDLE')

	// Allow user to change email only on specified states
	// to be able to show valid email during 'LOADING' state or any other
	// that do'nt supposed to allow to value be changed
	const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const { value } = event.target
		switch (state) {
			case 'IDLE':
			case 'INVALID_EMAIL':
			case 'MUST_PROVIDE_EMAIL':
				return setEmail(value)

			default:
				return
		}
	}

	// Delay layout change on submitting. May be we do not need to show "LOADING"
	// state so we will avoid unnecessary interface changes. See all tweets by
	// Dan Abramov about React Suspence
	useEffect(() => {
		if (state === 'SUBMITTING') {
			const timeout = setTimeout(() => {
				setState('LOADING')
			}, 150)
			return () => clearTimeout(timeout)
		}
	}, [state])

	const onError = (error: any) => {
		console.error(`Error during email mutation call`, error)
		setState('ERROR')
	}

	// Assume that all states is about invalid email except those that
	// explicitly tell that we are moving to the next step.
	const onCompleted = ({ auth }: Auth) => {
		const { step } = auth
		switch (step) {
			case 'NO_PASSWORD':
				setState('COMPLETED')
				onResponse(step)
				break
			default:
				setState('INVALID_EMAIL')
		}
	}

	const [sendEmail] = useMutation<Auth, { email: string }>(SEND_EMAIL, {
		onCompleted,
		onError,
	})

	return (
		<form
			onSubmit={e => {
				e.preventDefault()
				if (email) {
					setState('SUBMITTING')
					sendEmail({ variables: { email } })
				} else {
					setState('MUST_PROVIDE_EMAIL')
				}
			}}
		>
			{(() => {
				// TODO: add label and styles and id and name
				switch (state) {
					case 'LOADING':
						return (
							<input
								type="email"
								value={email}
								disabled
								style={{ border: '1px solid red' }}
							/>
						)
					case 'COMPLETED':
						return <p>{email}</p>
					case 'MUST_PROVIDE_EMAIL':
						return (
							<input
								type="email"
								value={email}
								required
								style={{ border: '1px solid blue' }}
								onChange={handleEmailChange}
							/>
						)
					case 'INVALID_EMAIL':
						return (
							<input
								type="email"
								value={email}
								required
								style={{ border: '1px solid green' }}
								onChange={handleEmailChange}
							/>
						)
					default:
						return (
							<input
								type="email"
								value={email}
								onChange={handleEmailChange}
								required
							/>
						)
				}
			})()}
		</form>
	)
}

export default SendEmail
