import React, { useState, useEffect } from 'react'

interface Props {
	url: string
	method: string
	children: (props: {
		status: string
		result: Record<string, any>
		restart: () => void
		cancel: () => void
	}) => React.ReactElement
	payload?: Record<string, any>
	headers?: Record<string, any>
	delay?: number
}

/**
 * This component is using render props and returns:
 *  1. current request status: 'IDLE', 'CANCELED', 'LOADING', 'COMPLETED', 'ERROR'
 *  2. result of request (error object if there is error)
 *  3. retry function - to start request over (previos one is canceled)
 *  4. cancel request funtion - to cancel current request
 *
 * Requests are cancels and restarts automaticaly on url, method or payload change
 * It uses fetch under the hood. By default headers set to handle JSONs.
 * You can set delay to delay status toggling for loading status. This is similar
 * to React Suspence and uses same delay time as React by default.
 */
const HttpRequest: React.FC<Props> = ({
	url,
	method,
	payload,
	headers,
	delay,
	children,
}) => {
	const [status, setStatus] = useState('IDLE')
	const [result, setResult] = useState<null | Record<string, any>>(null)
	const [state, setState] = useState(status)

	const restart = () => setState('RETRY')
	const cancel = () => setState('CANCEL')

	// Toggle status on state change
	useEffect(() => {
		// Delay status toggling only for LOADING state
		if (state === 'LOADING') {
			const timeout = setTimeout(() => {
				setStatus('LOADING')
			}, delay || 150)
			return () => clearTimeout(timeout)
			// For any other states toggle status imediately
		} else {
			setStatus(state)
		}
	}, [state, delay])

	useEffect(() => {
		let cancelRequest = false
		switch (state) {
			case 'IDLE':
			case 'RETRY':
				setState('LOADING')
				break

			case 'LOADING':
				fetch(url, {
					method,
					body: JSON.stringify(payload),
					headers: headers || {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
				})
					.then(response => {
						if (cancelRequest) {
							return setState('CANCELED')
						}
						return response.json()
					})
					.then(json => {
						if (cancelRequest) {
							return setState('CANCELED')
						}
						setResult(json)
						setState('COMPLETED')
					})
					.catch(error => {
						if (cancelRequest) {
							return setState('CANCELED')
						}
						setState('ERROR')
						setResult(error)
						console.error('Error during request to server', error)
					})
				break

			case 'CANCELED':
			case 'ERROR':
			default:
		}
		return () => {
			cancelRequest = true
		}
	}, [state, url, method, payload, headers])
	return children({
		status,
		result,
		restart,
		cancel,
	})
}

export default HttpRequest
