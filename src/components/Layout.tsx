import React from 'react'
import styled from '@emotion/styled'

import Seo from './Seo'

const Container = styled.div`
	max-width: 1200px;
	margin: auto;
`

interface Props {
	title: string
}

const Layout: React.FC<Props> = ({ title, children }) => {
	return (
		<>
			<Seo title={title} />
			<Container>{children}</Container>
		</>
	)
}

export default Layout
