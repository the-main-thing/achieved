import ApolloClient from 'apollo-boost'
import fetch from 'isomorphic-fetch'

export const client = new ApolloClient({
	// TODO: use .env for dev and prod builds
	uri: 'http://localhost:4000/graphql',
	fetch,
})
