import React, { useState } from 'react'
import { navigate } from 'gatsby'
import styled from '@emotion/styled'

import Layout from '../components/Layout'
import SendEmail from '../components/Auth'

const AuthContainer = styled.div`
	border: 1px solid dodgerblue;
	padding: 2em;
	font-size: 1.3em;
`

const AuthButton = styled.button`
	display: block;
	border: none;
	background: none;
	box-shadow: none;
	border: 1px solid dodgerblue;
	color: dodgerblue;
	&:hover {
		background-color: dodgerblue;
		color: white;
		transition: 0.2s;
	}
	&:focus {
		outline: 2px solid dodgerblue;
	}
	&:not(:hover) {
		transition: 0.5s;
	}
`

const CloseButton = styled.button`
	display: block;
	border: none;
	background: none;
	box-shadow: none;
	border: 1px solid dodgerblue;
	background: dodgerblue;
	color: white;
	&:hover {
		background: none;
		color: dodgerblue;
		transition: 0.2s;
	}
	&:focus {
		outline: 2px solid dodgerblue;
	}
	&:not(:hover) {
		transition: 0.5s;
	}
`

const IndexPage: React.FC = () => {
	const handleAuth = () => navigate('/app')
	const [state, setState] = useState('INITIAL')
	return <SendEmail onResponse={data => console.log({ data })} />
}
// 	return (
// 		<Layout title="Achieved app">
// 			{(() => {
// 				switch (state) {
// 					case 'INITIAL':
// 					case 'CLOSED':
// 					default:
// 						return (
// 							<AuthButton type="button" onClick={() => setState('OPENED')}>
// 								Войти
// 							</AuthButton>
// 						)
// 					case 'OPENED':
// 						return (
// 							<>
// 								<CloseButton type="button" onClick={() => setState('CLOSED')}>
// 									Закрыть
// 								</CloseButton>
// 								<AuthContainer>
// 									<Auth url="http://localhost:4000" onAuth={handleAuth} />
// 								</AuthContainer>
// 							</>
// 						)
// 				}
// 			})()}
// 		</Layout>
// 	)
// }

export default IndexPage
