import React, { useContext, useLayoutEffect } from 'react'
import { navigate } from 'gatsby'

import Layout from '../components/Layout'

const App: React.FC = () => {
	// const [loggedIn] = useContext(LoggedInContext)
	// useLayoutEffect(() => {
	// 	if (!loggedIn) {
	// 		navigate('/')
	// 	}
	// }, [loggedIn])

	return (
		<Layout title="Achieved app">
			<h1>APP</h1>
		</Layout>
	)
}

export default App
