import Typography from 'typography'

const typography = new Typography({
	baseFontSize: '18px',
	baseLineHeight: 1.66,
	// TODO: enable it back
	// googleFonts: [
	// 	{
	// 		name: 'Montserrat',
	// 		styles: ['700'],
	// 	},
	// 	{
	// 		name: 'Roboto',
	// 		styles: ['400', '700'],
	// 	},
	// ],
	// headerFontFamily: ['Montserrat', 'Helvetica', 'Arial', 'sans-serif'],
	// bodyFontFamily: ['Roboto', 'serif'],
})
export default typography
