module.exports = {
	parser: '@typescript-eslint/parser', // Specifies the ESLint parser
	extends: [
		'eslint:recommended',
		'plugin:react/recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier/@typescript-eslint',
		'plugin:prettier/recommended',
		'plugin:jsx-a11y/recommended',
	],
	settings: {
		// 'import/parsers': {
		// 	'@typescript-eslint/parser': ['.ts', '.tsx'],
		// },
		// 'import/resolver': {
		// 	typescript: {},
		// },
		react: {
			version: 'detect',
		},
	},
	env: {
		browser: true,
		node: true,
		es6: true,
		jest: true,
	},
	plugins: ['@typescript-eslint', 'react', 'jsx-a11y', 'react-hooks'],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
		sourceType: 'module', // Allows for the use of imports
	},
	rules: {
		'react/jsx-filename-extension': [
			2,
			{ extensions: ['.js', '.jsx', '.ts', '.tsx'] },
		],
		'react/prop-types': 'off', // Disable prop-types as we use TypeScript for type checking
		'@typescript-eslint/explicit-function-return-type': 'off',
		'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
		'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
		'@typescript-eslint/no-use-before-define': ['error', { functions: false }],
	},
	overrides: [
		// Override some TypeScript rules just for .js files
		{
			files: ['*.js'],
			rules: {
				'@typescript-eslint/no-var-requires': 'off', //
			},
		},
	],
}
