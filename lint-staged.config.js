module.exports = {
	'**/*.+(js|jsx|json|ts|tsx)': ['eslint --fix'],
	'**/*.+(js|jsx|json|less|css|scss|html|md|mdx|ts|tsx)': [
		'prettier --write',
		// 'jest --findRelatedTests',
		'git add',
	],
}
